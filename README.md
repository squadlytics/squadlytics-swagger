# Getting started

Open your terminal and run the following command

```bash
git clone git@bitbucket.org:squadlytics/squadlytics-swagger.git
cd squadlytics-swagger
./start-swagger.sh
```

This should start a new Docker daemon, load the swagger definition and open your browser at http://localhost:8090.

To kill the daemon simply run the following.

```bash
docker stop squad-swagger-ui
docker rm squad-swagger-ui
```
