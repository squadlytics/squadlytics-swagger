#!/bin/bash
docker stop squad-swagger-ui
docker rm squad-swagger-ui
docker run --name squad-swagger-ui -d -p 8090:8080 -e SWAGGER_JSON=/squadlytics-swagger/api_v1.yml -v $PWD:/squadlytics-swagger swaggerapi/swagger-ui
open http://localhost:8090